export class Pomodoro {

    public start: Date = new Date()
    public finish?: Date
   
    public type: PomodoroType
    public maxTime: Number

    constructor(type: PomodoroType) {
        this.type = type
        
        if (type == PomodoroType.TASK) {
            this.maxTime = 10000
        } else if (type == PomodoroType.BREAK) {
            this.maxTime = 5000
        } else {
            this.maxTime = 7000
        }
    }
}

export enum PomodoroType {
    TASK,
    BREAK,
    LONG_BREAK   
}